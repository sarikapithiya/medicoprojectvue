import { defineStore } from 'pinia';
import { useRouter } from "vue-router";

export const useDrawerStore = defineStore('drawer', {
  state: () => ({
    drawerVisible: false,
    title: '',
    childComponent: '',
    data: null,
    callback: null,
    router:useRouter(),
    
  }),
  getters: {
    getChildComponent() {
      return this.childComponent;
    },
  
  },
  actions: {
    openDrawer(payload) {
      this.drawerVisible =  true;
      this.title = payload.title;
      this.childComponent = payload.childComponent;
      this.data = payload.data;
      this.callback = payload.callback ?? null;
      
    },

    closeDrawer() {
      if (typeof this.callback === 'function') {
        this.callback();
      }
      this.drawerVisible = false;
      this.title = '';
      this.data = null;
      
      },
   
    goBack(){ 
      this.router.go(-1)
    },

    resetForm() {
      this.data = null;
    }

    
  },
});


