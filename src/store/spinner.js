import { defineStore } from 'pinia';
import { ref} from 'vue'

export const useSpinnerStore  = defineStore('spinner', ()=>{
    const spinning = ref(false)

    function startSpin(){
        spinning.value = true
        setTimeout(() => {
            spinning.value = false; 
          }, 5000);         
    }
    return{
        spinning,
        startSpin
    }
})

