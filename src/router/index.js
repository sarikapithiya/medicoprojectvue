import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../pages/HomeView.vue'
// import PatientsView from '../pages/Patients/PatientsView.vue'
import PatientsView from '../pages/Patients/IndexP.vue'
import userView from '../pages/Patients/Details/PatientView.vue'
import CaseHistoryView from '../pages/CaseHistory/IndexC.vue'
import PaymentsView from '../pages/Payments/IndexPy.vue'
import DoctorView from '../pages/Doctors/IndexD.vue'
import AddDoctor from '../pages/Doctors/AddDoctors.vue'
import ViewDoctor from '../pages/Doctors/ViewDoctor.vue'
import ContactsView from '../pages/Contacts/IndexCo.vue'
import AnnouncementsView from '../pages/Announcements/IndexA.vue'
import LoginUser from '../pages/auth/LoginUser.vue'
import RegisterUser from '../pages/auth/RegisterUser.vue'
import ResetPassword from '../pages/auth/ResetPassword.vue'
import LabReport from '../pages/Lab-reports/IndexL.vue'
import UserView from '../pages/users/IndexU.vue'
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      requiresAuth: true 
    }
  },
  
  {
    path: '/patients',
    name: 'patients',
    component: PatientsView
  },
  {
    path: '/userview/:id',
    name: 'userview',
    component: userView,

  },
  {
    path: '/case-history',
    name: 'case-history',
    component: CaseHistoryView
  },
  {
    path: '/payments',
    name: 'payments',
    component: PaymentsView
  },
  {
    path: '/doctor',
    name: 'doctor',
    component: DoctorView
  },
  {
    path: '/doctor/store/:id?',
    name: 'store',
    component: AddDoctor,
    props: true
  },
  {
    path: '/doctors/:id/Details',
    name: 'ViewDoctors',
    component: ViewDoctor,
    props: true,
  },
  {
    path: '/contacts',
    name: 'contacts',
    component: ContactsView
  },
  {
    path: '/announcements',
    name: 'announcements',
    component: AnnouncementsView
  },
  {
    path: '/login',
    name:'login',
    component:LoginUser
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterUser,
  },
  {
    path:'/reset-password',
    name:'reset-password',
    component:ResetPassword
  },
  {
    path:'/lab-reports',
    name:'lab-reports',
    component:LabReport
  },
  {
    path:'/users',
    name:'users',
    component:UserView
  }


]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})



router.beforeEach((to, from, next) => {
  const isLoggedIn = localStorage.getItem('isLoggedIn');
  
  if (to.meta.requiresAuth && !isLoggedIn) {
    next('/login'); 
  } else if ((to.name === 'login' || to.name === 'register') && isLoggedIn) {
    next('/'); 
  } else {
    next(); 
  }
});



export default router
